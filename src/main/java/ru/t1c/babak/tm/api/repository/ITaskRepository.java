package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

}
