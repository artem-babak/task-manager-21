package ru.t1c.babak.tm.api.model;

public interface IHaveName {

    String getName();

    void setName(String name);

}