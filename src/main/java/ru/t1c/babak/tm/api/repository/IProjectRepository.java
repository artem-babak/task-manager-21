package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

}
