package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractCommand {

    public static final String NAME = "user-change-password";

    public static final String DESCRIPTION = "Change user password.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("\tENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final String userId = getServiceLocator().getAuthService().getUserId();
        getServiceLocator().getUserService().setPassword(userId, password);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
