package ru.t1c.babak.tm.command.system;

import ru.t1c.babak.tm.util.InformationData;

public final class InfoCommand extends AbstractSystemCommand {

    public static final String NAME = "info";

    public static final String ARGUMENT = "-i";

    public static final String DESCRIPTION = "Show system info.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final InformationData freeMemoryData = new InformationData(freeMemory);
        System.out.println("Free memory: " + freeMemoryData);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final InformationData maxMemoryData = new InformationData(maxMemory);
        System.out.println("Maximum memory: " +
                (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryData));
        final long totalMemory = runtime.totalMemory();
        final InformationData totalMemoryData = new InformationData(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryData);
        final long usedMemory = totalMemory - freeMemory;
        final InformationData usedMemoryData = new InformationData(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryData);
    }

}
