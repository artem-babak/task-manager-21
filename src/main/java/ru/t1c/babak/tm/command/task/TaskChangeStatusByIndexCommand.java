package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-change-status-by-index";

    public static final String DESCRIPTION = "Change task status by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().findOneByIndex(index);
        System.out.println("\tENTER STATUS:");
        System.out.println("\t\t" + Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, status);
    }

}
