package ru.t1c.babak.tm.command.system;

import ru.t1c.babak.tm.api.model.ICommand;
import ru.t1c.babak.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandsCommand extends AbstractSystemCommand {

    public static final String NAME = "commands";

    public static final String ARGUMENT = "-cmd";

    public static final String DESCRIPTION = "Show application commands.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name + " : " + command.getDescription());
        }
    }

}
