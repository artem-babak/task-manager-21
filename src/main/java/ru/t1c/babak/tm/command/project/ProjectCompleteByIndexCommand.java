package ru.t1c.babak.tm.command.project;

import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-index";

    public static final String DESCRIPTION = "Complete project by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        renderAllProjects();
        System.out.println("\tENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

}
