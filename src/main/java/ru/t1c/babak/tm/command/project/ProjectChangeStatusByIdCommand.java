package ru.t1c.babak.tm.command.project;

import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-change-status-by-id";

    public static final String DESCRIPTION = "Change project status by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("\tENTER ID: ");
        final String id = TerminalUtil.nextLine();
        getProjectService().findOneById(id);
        System.out.println("\tENTER STATUS:");
        System.out.println("\t\t" + Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, status);
    }

}
