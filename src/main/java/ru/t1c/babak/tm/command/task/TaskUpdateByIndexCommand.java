package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-update-by-index";

    public static final String DESCRIPTION = "Update task by index.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");
        renderAllTasks();
        System.out.println("\tENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().findOneByIndex(index);
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().updateByIndex(userId, index, name, description);
    }

}
