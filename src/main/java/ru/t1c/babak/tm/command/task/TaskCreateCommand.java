package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public static final String NAME = "task-create";

    public static final String DESCRIPTION = "Create new task.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("\tENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("\tENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        System.out.println("\tENTER BEGIN DATE");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("\tENTER END DATE");
        final Date dateEnd = TerminalUtil.nextDate();
        final String userId = getUserId();
        getTaskService().create(userId, name, description, dateBegin, dateEnd);
    }

}
