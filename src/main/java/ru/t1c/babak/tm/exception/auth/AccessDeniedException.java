package ru.t1c.babak.tm.exception.auth;

import ru.t1c.babak.tm.exception.entity.AbstractEntityNotFoundException;

public final class AccessDeniedException extends AbstractEntityNotFoundException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
