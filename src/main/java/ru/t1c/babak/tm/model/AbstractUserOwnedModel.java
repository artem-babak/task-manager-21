package ru.t1c.babak.tm.model;

import ru.t1c.babak.tm.api.model.IHaveUserId;

public class AbstractUserOwnedModel extends AbstractModel implements IHaveUserId {

    private String userId;

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

}
