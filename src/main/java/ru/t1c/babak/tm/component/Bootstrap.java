package ru.t1c.babak.tm.component;

import ru.t1c.babak.tm.api.repository.ICommandRepository;
import ru.t1c.babak.tm.api.repository.IProjectRepository;
import ru.t1c.babak.tm.api.repository.ITaskRepository;
import ru.t1c.babak.tm.api.repository.IUserRepository;
import ru.t1c.babak.tm.api.service.*;
import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.command.project.*;
import ru.t1c.babak.tm.command.system.*;
import ru.t1c.babak.tm.command.task.*;
import ru.t1c.babak.tm.command.user.*;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.exception.system.ArgumentNotSupportedException;
import ru.t1c.babak.tm.exception.system.CommandNotSupportedException;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.model.User;
import ru.t1c.babak.tm.repository.CommandRepository;
import ru.t1c.babak.tm.repository.ProjectRepository;
import ru.t1c.babak.tm.repository.TaskRepository;
import ru.t1c.babak.tm.repository.UserRepository;
import ru.t1c.babak.tm.service.*;
import ru.t1c.babak.tm.util.DateUtil;
import ru.t1c.babak.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new AboutCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new ExitCommand());
        registry(new InfoCommand());
        registry(new CommandsCommand());
        registry(new ArgumentsCommand());
        registry(new TaskListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskAddToProjectByIdCommand());
        registry(new TaskRemoveFromProjectByIdCommand());
        registry(new ProjectListCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserViewProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateCommand());
        registry(new UserRegistryCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String[] args) {
        if (runArgument(args)) System.exit(0);
        initUsers();
        initData();
        //predefinedCommands();
        initLogger();
        while (true) {
            try {
                System.out.println("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                runCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER! **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        taskService.create(userService.findByLogin("admin").getId(), "3Demo_task-1", "first demo task");
        taskService.create(userService.findByLogin("admin").getId(), "1DEMO_TASK-2", "second demo task");
        taskService.create(userService.findByLogin("admin").getId(), "2demo_task-3");
        taskService.updateByIndex(userService.findByLogin("admin").getId(), 2, "2demo_task-3.1", "new description");
        projectService.create(userService.findByLogin("admin").getId(), "3proj-1", "first demo proj");
        projectService.create(userService.findByLogin("admin").getId(), "1proj-2", "second demo proj");
        projectService.create(userService.findByLogin("admin").getId(), "2proj-3");
        projectService.updateByIndex(userService.findByLogin("admin").getId(), 2, "2proj-3.1", "new description");
        projectService.add(new Project("1-1proj", Status.IN_PROGRESS, DateUtil.toDate("04.10.2019")));
        projectService.add(new Project("1-2proj", Status.NOT_STARTED, DateUtil.toDate("05.03.2018")));
        projectService.add(new Project("1-3proj", Status.IN_PROGRESS, DateUtil.toDate("16.02.2020")));
        projectService.add(new Project("1-4proj", Status.COMPLETED, DateUtil.toDate("22.01.2021")));
    }

    private void predefinedCommands() {
        runCommand("version");
        runCommand("task-list");
        runCommand("project-list");
//        runCommand("project-show-by-index");
//        runCommand("task-show-by-index");
//        runCommand("task-show-by-index");
//        runCommand("task-show-by-index");
//        runCommand("task-add-to-project-by-id");
//        runCommand("task-add-to-project-by-id");
//        runCommand("task-list-by-project-id");
    }

    public boolean runArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        runArgument(arg);
        return true;
    }

    public void runArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void runCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
